var request = require('supertest');
var server = require('../server');

describe('GET /', function() {
  it('should render ok', function(done) {
    request(server)
      .get('/')
      .expect(200, done);
  });
});

describe('GET YELP API IN JSON /places/:location?format=json', function() {
  it('should return json', function(done) {
    request(server)
      .get('/places/dublin?format=json')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});
