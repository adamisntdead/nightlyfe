const Bar = require('../models/Bar');
const User = require('../models/User');

exports.addGoing = function (req, res) {
    console.log(req.user);
    User.findOne({
        email: req.user.email
    }, function (err, user) {
        user.goingTo.push(req.params.barId);
        user.save(function () {
            var bar = new Bar();
            bar.barId = req.params.barId;
            bar.personGoing = req.user.email;
            bar.save(function (err) {
                if (err) {
                    console.log(err);
                }

                res.redirect('/places/');
            });
        });
    });
};

exports.removeGoing = function (req, res) {
    User.findOne({
        email: req.user.email
    }, function (err, user) {
        var index = user.goingTo.indexOf(req.params.barId);
        if (index > -1) {
            user.goingTo.splice(index, 1);
        }
        user.save(function () {
            Bar.find({
                barId: req.params.barId,
                personGoing: req.user.email
            }).remove(function () {
                res.redirect('/places/');
            });
        });
    });
};

exports.getGoing = function (barID, callback) {
    Bar.find({
        barId: barID
    }, function(err, bars) {
        callback(bars.length);
    });
    
};
    