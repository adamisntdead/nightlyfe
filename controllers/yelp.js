const Yelp = require('yelp');
const barC = require('./bar');

var yelp = new Yelp({
    consumer_key: process.env.YELP_CONSUMER_KEY,
    consumer_secret: process.env.YELP_CONSUMER_SECRET,
    token: process.env.YELP_TOKEN,
    token_secret: process.env.YELP_TOKEN_SECRET,
});

exports.getBars = function (req, res) {
    yelp.search({
        term: 'bars',
        location: req.params.location
    }, function (err, data) {
        if (err) throw err;

        function go(k, callback) {
            var nextIt = k + 1;
            console.log('Current it: ' + k + ' next it:' + nextIt);
            if (k == data.businesses.length) {
                if (req.query.format == 'json') {
                    return res.json(data);
                }
                else {
                    return res.render('search', {
                        title: 'Results',
                        result: true,
                        places: data.businesses,
                        location: req.params.location
                    });
                }
                return;
            }

            data.businesses[k].going = false;
            barC.getGoing(data.businesses[k].id, function (count) {
                data.businesses[k].count = count;
                if (!req.user) {
                    go(k + 1);
                }
                else {
                    for (var j = 0; j < req.user.goingTo.length; j++) {
                        if (data.businesses[k].id == req.user.goingTo[j]) {
                            data.businesses[k].going = true;
                        }
                    }
                    go(k + 1);
                }
            });
        }

        go(0);
    });
};
