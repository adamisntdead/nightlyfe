const yelpController = require('../controllers/yelp');
const userController = require('../controllers/user');
const barController = require('../controllers/bar');

module.exports = function (app) {
  app.get('/', landing);
  app.get('/places/:location', yelpController.getBars);
  app.get('/places/', search);
  app.get('/account', userController.ensureAuthenticated, userController.accountGet);
  app.put('/account', userController.ensureAuthenticated, userController.accountPut);
  app.delete('/account', userController.ensureAuthenticated, userController.accountDelete);
  app.get('/signup', userController.signupGet);
  app.post('/signup', userController.signupPost);
  app.get('/login', userController.loginGet);
  app.post('/login', userController.loginPost);
  app.get('/logout', userController.logout);
  app.get('/going/:barId', userController.ensureAuthenticated, barController.addGoing);
  app.get('/notgoing/:barId', userController.ensureAuthenticated, barController.removeGoing);

};

function landing(req, res) {
  if (req.user) {
    res.redirect('/places/');
    return;
  }
  res.render('landing');
}

function search(req, res) {
  res.render('search', {
    title: 'Search',
    redirect: true
  });
}
