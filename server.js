// Require Modules
const express = require('express');
const logger = require('morgan');
const compression = require('compression');
const path = require('path');
const sass = require('node-sass-middleware');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
const passport = require('passport');

// Load environment variables from .env file
require('dotenv-safe').load();

// Passport OAuth strategies
require('./config/passport');

// Setup Express App
const app = express();
mongoose.connect(process.env.MONGODB);
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(methodOverride('_method'));
app.use(session({ secret: process.env.SESSION_SECRET, resave: true, saveUninitialized: true }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});
app.use(logger('dev'));
app.use(
    sass({
      src: path.join(__dirname, 'sass'),
      dest: path.join(__dirname, 'public/css'),
      prefix: '/css',
      debug: false
    })
);
app.use(express.static(path.join(__dirname, 'public')));
require('./router/main.js')(app);

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}

// Start Server
app.listen(app.get('port'));

module.exports = app;
