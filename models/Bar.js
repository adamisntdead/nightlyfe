const mongoose = require('mongoose');

var barSchema = new mongoose.Schema({
  barId: String,
  personGoing: String
});

var Bar = mongoose.model('Bar', barSchema);

module.exports = Bar;
