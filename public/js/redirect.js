// Url Variables
var baseUrl = $(location).attr('protocol') + '//' + $(location).attr('host') + '/places/';


if (getSearch() != "") {
    window.location = baseUrl + getSearch();
}

// Get previous saves
function getSearch() {
    if (localStorage.lastSearch != undefined) {
        return localStorage.lastSearch;
    }
    else {
        return "";
    }
}