// Url Variables
var baseUrl = $(location).attr('protocol') + '//' + $(location).attr('host') + '/places/';


// This Function Handles the Search
function handle() {
    if ($('#search').val() != "") {
        saveSearch($('#search').val());
        window.location = baseUrl + $('#search').val();
    }
}

// Searches on Enter Keypress
$(document).keypress(function (e) {
    if (e.which == 13) {
        handle()
    }
});

// Checkes the searchbar isn't empty
$('#goSearch').on('click', function () {
    handle();
});

// Save the search to localstorage
function saveSearch(query) {
    localStorage.lastSearch = query;
}
